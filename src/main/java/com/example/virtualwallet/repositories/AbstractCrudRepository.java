package com.example.virtualwallet.repositories;

import com.example.virtualwallet.repositories.contracts.BaseCrudRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractCrudRepository<T> extends AbstractReadRepository<T> implements BaseCrudRepository<T> {

    private final SessionFactory sessionFactory;

    public AbstractCrudRepository(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public T create(T entity) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
            return entity;
        }
    }

    @Override
    public T update(T entity) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
            return entity;
        }
    }

    @Override
    public void delete(int id) {
        T toDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(toDelete);
            session.getTransaction().commit();
        }
    }


}
