package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.TransactionCategory;

import java.util.List;

public interface TransactionCategoryRepository extends BaseCrudRepository<TransactionCategory> {

    List<TransactionCategory> filter(FilterOptions filterOptions);

}
