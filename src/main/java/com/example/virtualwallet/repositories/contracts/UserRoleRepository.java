package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.UserRole;

public interface UserRoleRepository extends BaseCrudRepository<UserRole> {

}
