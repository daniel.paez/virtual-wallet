package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface TransferRepository extends BaseCrudRepository<Transfer> {

    Transfer create(Transfer transfer, Wallet wallet);

    List<Transfer> getAllUserTransfers(int userId);

}
