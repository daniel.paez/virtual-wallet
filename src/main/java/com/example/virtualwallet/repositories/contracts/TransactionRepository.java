package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface TransactionRepository extends BaseCrudRepository<Transaction> {

    //transactions filtered by period, recipient, and direction (incoming or outgoing) and sort them by amount and date
    List<Transaction> filterForAdmin(FilterOptions filterOptions);

    Transaction create(Transaction transaction, Wallet recipientWallet, Wallet senderWallet);


    //Get - list all transactions for wallet
    //users/{userId}/wallets/{walletId}/transactions

    List<Transaction> getAllWalletTransactions(int userId, int walletId);

    //Get - get specific transaction
    //users/{userId}/wallets/{walletId}/transactions/{transactionId}

    Transaction getWalletTransactionById(int userId, int walletId, int transactionId);

    //Post - post new transaction
    //users/{userId}/wallets/{walletId}/transactions

    //TODO: in service - create
    //Filter transaction
    //users/{userId}/wallets/{walletId}/transactions

    List<Transaction> filterUsersTransactions(int userId, FilterOptions filterOptions);

    List<Transaction> filterAllWalletTransactions(FilterOptions filterOptions, int walletId);

    List<Transaction> getAllUserInternalTransactions(int userId);

    Transaction createInternalTransaction(int userId,Transaction transaction,Wallet senderWallet,Wallet recipientWallet);

    List<Transaction> getAllUserTransactions(int userId);
}
