package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.User;

import java.util.List;

public interface UserRepository extends BaseCrudRepository<User> {

    User getByField(String value);

    User getActiveUserByField(String value);

    List<User> getAllActiveUsers();

    User getActiveUserById(int id);

    User findByVerificationCode(String code);


}
