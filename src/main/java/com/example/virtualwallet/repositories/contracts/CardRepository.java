package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Card;

import java.util.List;

public interface CardRepository extends BaseCrudRepository<Card> {

    List<Card> getAllUserCards(int userId);

    Card getUserCardById(int userId, int cardId);

}
