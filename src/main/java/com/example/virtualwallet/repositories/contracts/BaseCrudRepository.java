package com.example.virtualwallet.repositories.contracts;

public interface BaseCrudRepository<T> extends BaseReadRepository<T> {

    T create(T entity);

    T update(T entity);

    void delete(int id);
}
