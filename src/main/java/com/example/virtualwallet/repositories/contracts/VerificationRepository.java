package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Verification;

public interface VerificationRepository extends BaseCrudRepository<Verification> {

}
