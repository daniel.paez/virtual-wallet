package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface WalletRepository extends BaseCrudRepository<Wallet> {


    List<Wallet> getAllUserWallets(int userId);

    Wallet getUserWalletById(int userId, int walletId);

    Wallet getDefaultUserWallet(int userId);

    Wallet getUserWalletByName(int userId, String walletName);
}
