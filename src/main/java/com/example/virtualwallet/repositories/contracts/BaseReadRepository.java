package com.example.virtualwallet.repositories.contracts;

import java.util.List;

public interface BaseReadRepository<T> {

    <V> T getByField(String name, V value);

    T getById(int id);

    List<T> getAll();


}
