package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl extends AbstractCrudRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public User getByField(String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where (username = :value ) OR " +
                    "(phoneNumber = :value) OR (email =:value)", User.class);
            query.setParameter("value", value);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username, email or phone number", value);
            }
            return query.list().get(0);
        }
    }

    @Override
    public User getActiveUserByField(String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where (username = :value ) OR " +
                    "(phoneNumber = :value) OR (email =:value) and isActive = true", User.class);
            query.setParameter("value", value);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username, email or phone number", value);
            }
            return query.list().get(0);
        }
    }

    @Override
    public User getActiveUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where id=:id and isActive = true", User.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", id);
            }
            return query.list().get(0);
        }
    }

    @Override
    public User findByVerificationCode(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where verificationCode =:code", User.class);
            query.setParameter("code", code);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "verification code", code);
            }
            return query.list().get(0);
        }
    }


    @Override
    public List<User> getAllActiveUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where isActive = true ", User.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("There are no registered users yet");
            }
            return query.list();
        }
    }


//TODO: after manyToMany relations => add gettAllCards and getAllWallets methods
}

