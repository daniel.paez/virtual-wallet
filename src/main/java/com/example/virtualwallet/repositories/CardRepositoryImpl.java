package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardRepositoryImpl extends AbstractCrudRepository<Card> implements CardRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        super(Card.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Card> getAllUserCards(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Card> query = session.createQuery("from Card where owner.id =:userId");
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public Card getUserCardById(int userId, int cardId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Card> query = session.createQuery("from Card where owner.id =:userId and id =:cardId");
            query.setParameter("userId", userId);
            query.setParameter("cardId", cardId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Card", cardId);
            }
            return query.list().get(0);
        }
    }
}
