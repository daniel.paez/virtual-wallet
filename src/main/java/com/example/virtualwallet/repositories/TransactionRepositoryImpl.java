package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository
public class TransactionRepositoryImpl extends AbstractCrudRepository<Transaction> implements TransactionRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        super(Transaction.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Transaction> filterForAdmin(FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Transaction where 1=1");

            if (filterOptions.getStartDate().isPresent()) {
                queryString.append(" and date >= :fromDate");
            }
            if (filterOptions.getEndDate().isPresent()) {
                queryString.append(" and date <= :toDate");
            }
            if (filterOptions.getSenderUsername().isPresent()) {
                queryString.append(" and senderWallet.owner.username like :sender");
            }
            if (filterOptions.getRecipientUsername().isPresent()) {
                queryString.append(" and recipientWallet.owner.username like :recipient");
            }

            addSorting(filterOptions, queryString);

            Query<Transaction> query = session.createQuery(queryString.toString(), Transaction.class);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            if (filterOptions.getStartDate().isPresent()) {
                LocalDate localDate = LocalDate.parse(String.format(filterOptions.getStartDate().get()), formatter);
                query.setParameter("startDate", localDate);
            }
            if (filterOptions.getEndDate().isPresent()) {
                LocalDate localDate = LocalDate.parse(String.format(filterOptions.getEndDate().get()), formatter);
                query.setParameter("endDate", localDate);
            }
            if (filterOptions.getSenderUsername().isPresent()) {
                query.setParameter("senderUsername", String.format("%%%s%%", filterOptions.getSenderUsername().get()));
            }
            if (filterOptions.getRecipientUsername().isPresent()) {
                query.setParameter("recipientUsername", String.format("%%%s%%", filterOptions.getRecipientUsername().get()));
            }

            return query.list();

        }
    }


    private void addSorting(FilterOptions filterOptions, StringBuilder queryString) {
        if (filterOptions.getSortBy().isEmpty()) {
            return;
        }
        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "date":
                orderBy = "date";
                break;
            case "amount":
                orderBy = "amount";
                break;
            default:
                return;
        }
        queryString.append(String.format(" order by %s ", orderBy));

        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }

    @Override
    public Transaction create(Transaction transaction, Wallet recipientWallet, Wallet senderWallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(transaction);
            session.update(recipientWallet);
            session.update(senderWallet);
            session.getTransaction().commit();
            return transaction;
        }
    }


    @Override
    public List<Transaction> getAllWalletTransactions(int userId, int walletId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where senderWallet.id =:walletId or recipientWallet.id=:walletId");
            query.setParameter("walletId", walletId);
//
            return query.list();
        }
    }

    public List<Transaction> getAllUserTransactions(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where senderWallet.owner.id =:userId or recipientWallet.owner.id=:userId");
            query.setParameter("userId", userId);
//
            return query.list();
        }
    }

    @Override
    public Transaction getWalletTransactionById(int userId, int walletId, int transactionId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where (senderWallet.id =:walletId or recipientWallet.id=:walletId) and id=:transactionId");
            query.setParameter("walletId", walletId);
            query.setParameter("transactionId", transactionId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("There is no transaction with %s id in this wallet", transactionId));
            }
            return query.list().get(0);
        }
    }


    @Override
    public List<Transaction> filterUsersTransactions(int userId, FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Transaction where");

            //first check for direction - outgoing
            if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("outgoing")) {
                queryString.append(" senderWallet.owner.id = :userId");
                if (filterOptions.getStartDate().isPresent()) {
                    queryString.append(" and date >= :startDate");
                }
                if (filterOptions.getEndDate().isPresent()) {
                    queryString.append(" and date <= :endDate");
                }
                if (filterOptions.getRecipientUsername().isPresent()) {
                    queryString.append(" and recipientWallet.owner.username like :recipientUsername");
                }
                //then check for direction - incoming
            } else if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("incoming")) {
                queryString.append(" recipientWallet.owner.id=:userId");
                if (filterOptions.getStartDate().isPresent()) {
                    queryString.append(" and date >= :startDate");
                }
                if (filterOptions.getEndDate().isPresent()) {
                    queryString.append(" and date <= :endDate");
                }
                if (filterOptions.getSenderUsername().isPresent()) {
                    queryString.append(" and senderWallet.owner.username like :senderUsername");
                }

                //in all other cases
            } else {
                queryString.append(" (senderWallet.owner.id =:userId OR recipientWallet.owner.id =:userId)");
                if (filterOptions.getStartDate().isPresent()) {
                    queryString.append(" and date >= :startDate");
                }
                if (filterOptions.getEndDate().isPresent()) {
                    queryString.append(" and date <= :endDate");
                }
                if (filterOptions.getRecipientUsername().isPresent()) {
                    queryString.append(" and recipientWallet.owner.username like :recipientUsername");
                }
                if (filterOptions.getSenderUsername().isPresent()) {
                    queryString.append(" and senderWallet.owner.username like :senderUsername");
                }

            }


            addSorting(filterOptions, queryString);

            Query<Transaction> query = session.createQuery(queryString.toString(), Transaction.class);
            query.setParameter("userId", userId);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            if (filterOptions.getStartDate().isPresent()) {
                LocalDate localDate = LocalDate.parse(String.format(filterOptions.getStartDate().get()), formatter);

                query.setParameter("startDate", localDate);
            }
            if (filterOptions.getEndDate().isPresent()) {
                LocalDate localDate = LocalDate.parse(String.format(filterOptions.getEndDate().get()), formatter);
                query.setParameter("endDate", localDate);
            }
            if (filterOptions.getRecipientUsername().isPresent()) {
                query.setParameter("recipientUsername", String.format("%%%s%%", filterOptions.getRecipientUsername().get()));
            }
            if (filterOptions.getSenderUsername().isPresent()) {
                query.setParameter("senderUsername", String.format("%%%s%%", filterOptions.getSenderUsername().get()));
            }
            if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("incoming")) {
                query.setParameter("userId", userId);
            }
            if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("outgoing")) {
                query.setParameter("userId", userId);

            }

            return query.list();
        }
    }

    @Override
    public List<Transaction> filterAllWalletTransactions(FilterOptions filterOptions, int walletId) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Transaction where");

            //first check for direction - outgoing
            if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("outgoing")) {
                queryString.append(" senderWallet.id = :walletId");
                if (filterOptions.getStartDate().isPresent()) {
                    queryString.append(" and date >= :startDate");
                }
                if (filterOptions.getEndDate().isPresent()) {
                    queryString.append(" and date <= :endDate");
                }
                if (filterOptions.getRecipientUsername().isPresent()) {
                    queryString.append(" and recipientWallet.owner.username like :recipientUsername");
                }
                //then check for direction - incoming
            } else if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("incoming")) {
                queryString.append(" recipientWallet.id=:walletId");
                if (filterOptions.getStartDate().isPresent()) {
                    queryString.append(" and date >= :startDate");
                }
                if (filterOptions.getEndDate().isPresent()) {
                    queryString.append(" and date <= :endDate");
                }
                if (filterOptions.getSenderUsername().isPresent()) {
                    queryString.append(" and senderWallet.owner.username like :senderUsername");
                }

                //in all other cases
            } else {
                queryString.append(" (senderWallet.id =:walletId OR recipientWallet.id =:walletId)");
                if (filterOptions.getStartDate().isPresent()) {
                    queryString.append(" and date >= :startDate");
                }
                if (filterOptions.getEndDate().isPresent()) {
                    queryString.append(" and date <= :endDate");
                }
                if (filterOptions.getRecipientUsername().isPresent()) {
                    queryString.append(" and recipientWallet.owner.username like :recipientUsername");
                }
                if (filterOptions.getSenderUsername().isPresent()) {
                    queryString.append(" and senderWallet.owner.username like :senderUsername");
                }

            }


            addSorting(filterOptions, queryString);

            Query<Transaction> query = session.createQuery(queryString.toString(), Transaction.class);
            query.setParameter("walletId", walletId);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            if (filterOptions.getStartDate().isPresent()) {
                LocalDate localDate = LocalDate.parse(String.format(filterOptions.getStartDate().get()), formatter);

                query.setParameter("startDate", localDate);
            }
            if (filterOptions.getEndDate().isPresent()) {
                LocalDate localDate = LocalDate.parse(String.format(filterOptions.getEndDate().get()), formatter);
                query.setParameter("endDate", localDate);
            }
            if (filterOptions.getRecipientUsername().isPresent()) {
                query.setParameter("recipientUsername", String.format("%%%s%%", filterOptions.getRecipientUsername().get()));
            }
            if (filterOptions.getSenderUsername().isPresent()) {
                query.setParameter("senderUsername", String.format("%%%s%%", filterOptions.getSenderUsername().get()));
            }
            if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("incoming")) {
                query.setParameter("walletId", walletId);
            }
            if (filterOptions.getDirection().isPresent() && filterOptions.getDirection().get().equals("outgoing")) {
                query.setParameter("walletId", walletId);

            }

            return query.list();
        }
    }

    @Override
    public List<Transaction> getAllUserInternalTransactions(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where senderWallet.owner.id =:userId AND recipientWallet.owner.id=:userId");
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public Transaction createInternalTransaction(int userId,Transaction transaction,Wallet senderWallet,Wallet recipientWallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(transaction);
            session.update(recipientWallet);
            session.update(senderWallet);
            session.getTransaction().commit();
            return transaction;
        }
    }
}
