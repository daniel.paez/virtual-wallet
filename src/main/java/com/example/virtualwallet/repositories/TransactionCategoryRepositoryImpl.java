package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.TransactionCategory;
import com.example.virtualwallet.repositories.contracts.TransactionCategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TransactionCategoryRepositoryImpl extends AbstractCrudRepository<TransactionCategory>
        implements TransactionCategoryRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public TransactionCategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(TransactionCategory.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<TransactionCategory> filter(FilterOptions filterOptions) {
        //TODO:implement later- filter by direction(sender, recipient)
        return null;
    }
}
