package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.TransferRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TransferRepositoryImpl extends AbstractCrudRepository<Transfer> implements TransferRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TransferRepositoryImpl(SessionFactory sessionFactory) {
        super(Transfer.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Transfer create(Transfer transfer, Wallet wallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(transfer);
            session.update(wallet);
            session.getTransaction().commit();
            return transfer;
        }
    }

    @Override
    public List<Transfer> getAllUserTransfers(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transfer> query = session.createQuery("from Transfer where wallet.owner.id =:userId");
            query.setParameter("userId", userId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("There are no transfer records");
            }
            return query.list();
        }
    }
}
