package com.example.virtualwallet.repositories;


import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WalletRepositoryImpl extends AbstractCrudRepository<Wallet> implements WalletRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        super(Wallet.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Wallet> getAllUserWallets(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Wallet> query = session.createQuery("from Wallet where owner.id =:userId");
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public Wallet getUserWalletById(int userId, int walletId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Wallet> query = session.createQuery("from Wallet where owner.id =:userId and id =:walletId");
            query.setParameter("userId", userId);
            query.setParameter("walletId", walletId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Wallet", walletId);
            }
            return query.list().get(0);
        }
    }

    @Override
    public Wallet getDefaultUserWallet(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Wallet> query = session.createQuery("from Wallet where owner.id =:userId and isDefault =true");
            query.setParameter("userId", userId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User has no default wallet");
            }
            return query.list().get(0);
        }
    }

    @Override
    public Wallet getUserWalletByName(int userId, String walletName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Wallet> query = session.createQuery("from Wallet where owner.id =:userId and name =:walletName");
            query.setParameter("userId", userId);
            query.setParameter("walletName", walletName);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("User has no wallet with %s name", walletName));
            }
            return query.list().get(0);
        }
    }
}
