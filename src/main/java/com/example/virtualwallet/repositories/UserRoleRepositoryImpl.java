package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.UserRole;
import com.example.virtualwallet.repositories.contracts.UserRoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class UserRoleRepositoryImpl extends AbstractCrudRepository<UserRole> implements UserRoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRoleRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


}
