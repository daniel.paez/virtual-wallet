package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.Verification;
import com.example.virtualwallet.repositories.contracts.VerificationRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationRepositoryImpl extends AbstractCrudRepository<Verification> implements VerificationRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VerificationRepositoryImpl(SessionFactory sessionFactory) {
        super(Verification.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


}
