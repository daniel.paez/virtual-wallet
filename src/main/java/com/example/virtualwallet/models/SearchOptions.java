package com.example.virtualwallet.models;

import java.util.Optional;

public class SearchOptions {
    private Optional<String> username;
    private Optional<String> email;
    private Optional<String> phoneNumber;

    public SearchOptions(Optional<String> username, Optional<String> email, Optional<String> phoneNumber) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Optional<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
