package com.example.virtualwallet.models.dtos;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CardDto {

    @NotNull(message = "Card number can not be empty")
    @Size(min = 16, max = 16, message = "Card number should be 16 digits")
    private String cardNumber;

    @NotNull(message = "Check number can not be empty")
    @Size(min = 3, max = 3, message = "Check number should be 3 digits")
    private String checkNumber;

    //TODO: refactor cardType to a CardType class
    @NotNull(message = "Card type should be either Credit or Debit")
    private String cardType;

    @NotNull
    @Size(min = 5, max = 40, message = "Card holder name should be between 5 and 40 symbols")
    private String cardHolder;



    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "mm/yy")
    private String expireDate;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }
}
