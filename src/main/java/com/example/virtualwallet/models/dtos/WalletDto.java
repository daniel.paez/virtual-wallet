package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class WalletDto {

    @NotNull
    @Size(min = 5, max = 30, message = "Wallet name should be between 5 and 30 symbols")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
