package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;

public class RegisterDto extends UserDto {

    @NotEmpty(message = "Password confirmation can't be empty")
    private String passwordConfirm;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
