package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class TransactionDto {

    @NotNull
    @Positive
    private int amount;

    @NotNull
    private String walletName;

    @NotNull
    @Positive
    private int categoryId;

    @NotNull
    private String recipientInfo;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getRecipientInfo() {
        return recipientInfo;
    }

    public void setRecipientInfo(String recipientInfo) {
        this.recipientInfo = recipientInfo;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }
}
