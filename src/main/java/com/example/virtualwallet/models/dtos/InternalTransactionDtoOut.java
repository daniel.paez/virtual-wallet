package com.example.virtualwallet.models.dtos;

import java.util.Date;

public class InternalTransactionDtoOut {

    private int amount;

    private String outgoingWalletName;

    private String incomingWalletName;

    private Date date;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getOutgoingWalletName() {
        return outgoingWalletName;
    }

    public void setOutgoingWalletName(String outgoingWalletName) {
        this.outgoingWalletName = outgoingWalletName;
    }

    public String getIncomingWalletName() {
        return incomingWalletName;
    }

    public void setIncomingWalletName(String incomingWalletName) {
        this.incomingWalletName = incomingWalletName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
