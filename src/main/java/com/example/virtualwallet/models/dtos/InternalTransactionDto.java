package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class InternalTransactionDto {

    @NotNull
    @Positive
    private int amount;

    @NotNull
    private int outgoingWalletId;

    @NotNull
    private int incomingWalletId;

    @NotNull
    private int categoryId;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getOutgoingWalletId() {
        return outgoingWalletId;
    }

    public void setOutgoingWalletId(int outgoingWalletId) {
        this.outgoingWalletId = outgoingWalletId;
    }

    public int getIncomingWalletId() {
        return incomingWalletId;
    }

    public void setIncomingWalletId(int incomingWalletId) {
        this.incomingWalletId = incomingWalletId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
