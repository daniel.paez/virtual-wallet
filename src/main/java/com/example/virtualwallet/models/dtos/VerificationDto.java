package com.example.virtualwallet.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Lob;

public class VerificationDto {

    @Lob
    @Column(name = "id_card_photo")
    private MultipartFile idCardPhoto;

    @Lob
    @Column(name = "selfie")
    private MultipartFile selfie;

    public MultipartFile getIdCardPhoto() {
        return idCardPhoto;
    }

    public void setIdCardPhoto(MultipartFile idCardPhoto) {
        this.idCardPhoto = idCardPhoto;
    }

    public MultipartFile getSelfie() {
        return selfie;
    }

    public void setSelfie(MultipartFile selfie) {
        this.selfie = selfie;
    }
}
