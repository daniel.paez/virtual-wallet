package com.example.virtualwallet.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.validation.constraints.*;

public class UserDto {

    @NotNull
    @Size(min = 5, max = 30, message = "First name should be between 5 and 30 symbols")
    private String firstName;
    @NotNull
    @Size(min = 5, max = 30, message = "Last name should be between 5 and 30 symbols")
    private String lastName;

    @Email(message = "Email is not valid. Email should contain only lower case letters " +
            "and could contain digits. For example: firstname1.lastname@example.com ", regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}")
    @NotEmpty(message = "Email cannot be empty")
    private String email;

    @NotNull
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols")
    private String username;

    @Lob
    @Column(name = "photo")
    private MultipartFile photo;

    @NotNull
    @Pattern(message = "Password should contain: " +
            "Min 1 uppercase letter. " +
            "Min 1 lowercase letter. " +
            "Min 1 special character. " +
            "Min 1 number. " +
            "Min 8 characters. " +
            "Max 30 characters.", regexp = "^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,30}$")
    private String password;
    @NotNull(message = "Phone number cannot be empty")
    @Size(min = 10, max = 10, message = "Phone number should be 10 symbols")
    private String phoneNumber;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }
}
