package com.example.virtualwallet.models.dtos;

import java.util.Date;

public class TransactionDtoOut {

    private int amount;

    private Date date;

    private UserDtoOut sender;

    private UserDtoOut recipient;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserDtoOut getSender() {
        return sender;
    }

    public void setSender(UserDtoOut sender) {
        this.sender = sender;
    }

    public UserDtoOut getRecipient() {
        return recipient;
    }

    public void setRecipient(UserDtoOut recipient) {
        this.recipient = recipient;
    }
}
