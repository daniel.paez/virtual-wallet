package com.example.virtualwallet.models;

import java.util.Optional;

public class FilterOptions {

    private final Optional<String> startDate;

    private final Optional<String> endDate;

    private final Optional<String> senderUsername;

    private final Optional<String> recipientUsername;

    private final Optional<String> direction;
    private final Optional<String> sortBy;
    private final Optional<String> sortOrder;

    public FilterOptions(Optional<String> startDate,
                         Optional<String> endDate,
                         Optional<String> senderUsername,
                         Optional<String> recipientUsername,
                         Optional<String> direction, Optional<String> sortBy,
                         Optional<String> sortOrder) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.senderUsername = senderUsername;
        this.recipientUsername = recipientUsername;
        this.direction = direction;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public Optional<String> getStartDate() {
        return startDate;
    }

    public Optional<String> getEndDate() {
        return endDate;
    }

    public Optional<String> getSenderUsername() {
        return senderUsername;
    }

    public Optional<String> getRecipientUsername() {
        return recipientUsername;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public Optional<String> getDirection() {
        return direction;
    }
}
