package com.example.virtualwallet.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "verification_statuses")
public class VerificationStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "verification_status_id")
    private int id;

    @Column(name = "verification_status")
    private String status;

    public VerificationStatus() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VerificationStatus that = (VerificationStatus) o;
        return status.equals(that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}
