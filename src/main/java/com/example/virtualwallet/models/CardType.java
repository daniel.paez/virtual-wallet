//package com.example.virtualwallet.models;
//
//import javax.persistence.*;
//import java.util.Objects;
//
//@Entity
//@Table(name = "card_types")
//public class CardType {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "card_type_id")
//    private int id;
//
//    @Column(name = "card_type")
//    private String cardType;
//
//
//    public CardType() {
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getCardType() {
//        return cardType;
//    }
//
//    public void setCardType(String cardType) {
//        this.cardType = cardType;
//    }
//
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        CardType cardType1 = (CardType) o;
//        return cardType.equals(cardType1.cardType);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(cardType);
//    }
//}
