package com.example.virtualwallet.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "verifications")
public class Verification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "verification_id")
    private int id;

    @Column(name = "verification_done_date")
    private Date verificationDoneDate;

    @Column(name = "verification_request_date")
    private Date verificationRequestDate;
    

    //SENT, IN PROGRESS, APPROVED/REJECTED, RETURNED FOR UPDATE
    @ManyToOne
    @JoinColumn(name = "verification_status_id")
    private VerificationStatus status;

    @ManyToOne
    @JoinColumn(name = "admin_id")
    private User admin;

    @Lob
    @Column(name = "id_card_photo")
    private String idCardPhoto;

    @Lob
    @Column(name = "selfie")
    private String selfie;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Verification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getVerificationDoneDate() {
        return verificationDoneDate;
    }

    public void setVerificationDoneDate(Date verificationDoneDate) {
        this.verificationDoneDate = verificationDoneDate;
    }

    public Date getVerificationRequestDate() {
        return verificationRequestDate;
    }

    public void setVerificationRequestDate(Date verificationRequestDate) {
        this.verificationRequestDate = verificationRequestDate;
    }

    public VerificationStatus getStatus() {
        return status;
    }

    public void setStatus(VerificationStatus status) {
        this.status = status;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public String getIdCardPhoto() {
        return idCardPhoto;
    }

    public void setIdCardPhoto(String idCardPhoto) {
        this.idCardPhoto = idCardPhoto;
    }

    public String getSelfie() {
        return selfie;
    }

    public void setSelfie(String selfie) {
        this.selfie = selfie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
