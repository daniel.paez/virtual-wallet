package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.NotEnoughBalanceException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Currency;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    public static final String NOT_ENOUGH_BALANCE_ERROR = "Not enough balance in your wallet";
    public static final String USER_BLOCKED_ERROR = "Blocked users cannot make a transaction";

    public static final String ADMIN_FILTER_ERROR = "Only admin can access and filter all user's transactions";
    public static final String WALLET_ACCESS_FILTER_ERROR = "Only the wallet owner can access and filter the wallet's transactions.";
    public static final String ACCESS_ALL_TRANSACTIONS = "Only admins can access all transactions";
    public static final String ONLY_ADMINS_CAN_ACCESS_THE_TRANSACTION = "Only admins can access the transaction";
    public static final String ACCESS_ANOTHER_USER_S_INTERNAL_TRANSACTIONS = "You can not access another user's internal transactions.";
    public static final String CREATE_INTERNAL_TRANSACTION_FOR_ANOTHER_USER = "You can not create internal transaction for another user";
    public static final String BALANCE_TO_MAKE_INTERNAL_TRANSACTION = "You don't have enough balance to make internal transaction.";
    public static final int LARGE_SUM = 10000;
    private final TransactionRepository transactionRepository;

    private final WalletService walletService;


    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, WalletService walletService) {
        this.transactionRepository = transactionRepository;

        this.walletService = walletService;
    }


    @Override
    public List<Transaction> filter(FilterOptions filterOptions, User user) {
        if (!user.getUserRole().getRoleType().equals("admin")) {
            throw new UnauthorizedOperationException(ADMIN_FILTER_ERROR);
        }
        return transactionRepository.filterForAdmin(filterOptions);
    }

    @Override
    public List<Transaction> filterAllWalletTransactions(FilterOptions filterOptions, User user, int userId, int walletId) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(WALLET_ACCESS_FILTER_ERROR);
        }
        return transactionRepository.filterAllWalletTransactions(filterOptions, walletId);
    }


    @Override
    public List<Transaction> filterUserTransactions(FilterOptions filterOptions, User user, int userId) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(WALLET_ACCESS_FILTER_ERROR);
        }
        return transactionRepository.filterUsersTransactions(user.getId(), filterOptions);
    }

    @Override
    public List<Transaction> getAll(User user) {
        if (!user.getUserRole().getRoleType().equals("admin")) {
            throw new UnauthorizedOperationException(ACCESS_ALL_TRANSACTIONS);
        }
        return transactionRepository.getAll();
    }

    @Override
    public Transaction getById(User user, int id) {
        if (!user.getUserRole().getRoleType().equals("admin")) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_CAN_ACCESS_THE_TRANSACTION);
        }
        return transactionRepository.getById(id);
    }

    @Override
    public Transaction create(int userId, int walletId, Transaction transaction, User user) {
        walletService.getUserWalletById(user, userId, walletId);

        if (transaction.getAmount()>= LARGE_SUM){
                transaction.setLarge(true);
                transaction.setVerified(false);

                transactionRepository.create(transaction);
                //if isVerified- execute- new method for transfering balance
            //add boolean isExecuted, maybe remove isLarge
        }

        Wallet recipientWallet = transaction.getRecipientWallet();
        Wallet senderWallet = transaction.getSenderWallet();
        int senderWalletBalance = senderWallet.getBalance();
        int recipientWalletBalance = recipientWallet.getBalance();
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException(USER_BLOCKED_ERROR);
        }
        if (senderWalletBalance < transaction.getAmount()) {
            throw new NotEnoughBalanceException(NOT_ENOUGH_BALANCE_ERROR);
        }

        recipientWallet.setBalance(recipientWalletBalance += transaction.getAmount());
        senderWallet.setBalance(senderWalletBalance -= transaction.getAmount());

        return transactionRepository.create(transaction, recipientWallet, senderWallet);

    }


    @Override
    public List<Transaction> getAllWalletTransactions(User user, int userId, int walletId) {
        walletService.getUserWalletById(user, userId, walletId);
        return transactionRepository.getAllWalletTransactions(userId, walletId);

    }

    @Override
    public Transaction getWalletTransactionById(User user, int userId, int walletId, int transactionId) {
        walletService.getUserWalletById(user, userId, walletId);
        return transactionRepository.getWalletTransactionById(userId, walletId, transactionId);
    }

    @Override
    public List<Transaction> getAllUserInternalTransactions(User user, int userId) {
        if (user.getId()!=userId){
            throw new UnauthorizedOperationException(ACCESS_ANOTHER_USER_S_INTERNAL_TRANSACTIONS);
        }
        return transactionRepository.getAllUserInternalTransactions(userId);
    }

    @Override
    public Transaction createInternalTransaction(User user, int userId, Transaction transaction) {
        if (user.getId()!=userId){
            throw new UnauthorizedOperationException(CREATE_INTERNAL_TRANSACTION_FOR_ANOTHER_USER);
        }
        if (transaction.getSenderWallet().getBalance()<transaction.getAmount()){
            throw new NotEnoughBalanceException(BALANCE_TO_MAKE_INTERNAL_TRANSACTION);
        }
        Wallet senderWallet=transaction.getSenderWallet();
        Wallet recipientWallet=transaction.getRecipientWallet();
        senderWallet.setBalance(senderWallet.getBalance()- transaction.getAmount());
        recipientWallet.setBalance(recipientWallet.getBalance()+ transaction.getAmount());
        return transactionRepository.createInternalTransaction(userId,transaction,senderWallet,recipientWallet);
    }

    @Override
    public List<Transaction> getAllUserTransactions(int userId) {
        //TODO: add authentication
        return transactionRepository.getAllUserTransactions(userId);
    }

    //TODO: currency conversion to be implemented, waiting for an API Key from the website owner


//    public double getRateFromApi(Currency from, Currency to)
//            throws IOException {
//        String queryPath
//                = "https://free.currconv.com/api/v7/convert?q="
//                + from.getCurrencyCode() + "_"
//                + to.getCurrencyCode()
//                + "&compact=ultra&apiKey=" + API_KEY;
//        URL queryURL = new URL(queryPath);
//        HttpURLConnection connection
//                = (HttpURLConnection) queryURL.openConnection();
//        connection.setRequestMethod("GET");
//        connection.setRequestProperty("User-Agent", USER_AGENT_ID);
//        int responseCode = connection.getResponseCode();
//        if (responseCode == 200) { // 200 is HTTP status OK
//            InputStream stream
//                    = (InputStream) connection.getContent();
//            Scanner scanner = new Scanner(stream);
//            String quote = scanner.nextLine();
//            String number = quote.substring(quote.indexOf(':') + 1,
//                    quote.indexOf('}'));
//            return Double.parseDouble(number);
//        } else {
//            String excMsg = "Query " + queryPath
//                    + " returned status " + responseCode;
//            throw new RuntimeException(excMsg);
//        }
//    }

//    private void sendVerificationEmail(User user, String siteURL)
//            throws MessagingException, UnsupportedEncodingException {
//        String toAddress = user.getEmail();
//        String fromAddress = "virtual.wallet.project10@gmail.com";
//        String senderName = "Virtual Wallet Project";
//        String subject = "Please verify your registration";
//        String content = "Dear [[name]],<br>"
//                + "Please click the link below to verify your registration:<br>"
//                + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>"
//                + "Thank you,<br>"
//                + "Your virtual wallet team.";
//
//        MimeMessage message = mailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message);
//
//        helper.setFrom(fromAddress, senderName);
//        helper.setTo(toAddress);
//        helper.setSubject(subject);
//
//        content = content.replace("[[name]]", user.getFullName());
//        String verifyURL = siteURL + "/verify?code=" + user.getVerificationCode();
//
//        content = content.replace("[[URL]]", verifyURL);
//
//        helper.setText(content, true);
//
//        mailSender.send(message);
//
//        System.out.println("Email has been sent");
//    }
//
//    public boolean verify(String verificationCode) {
//        User user = userRepository.findByVerificationCode(verificationCode);
//
//        if (user == null || user.isEnabled()) {
//            return false;
//        } else {
//            user.setVerificationCode(null);
//            user.setEnabled(true);
//            userRepository.update(user);
//
//            return true;
//        }
//    }

}
