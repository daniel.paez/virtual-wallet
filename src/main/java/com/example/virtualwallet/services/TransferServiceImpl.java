package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.NotEnoughBalanceException;
import com.example.virtualwallet.exceptions.NotSupportedOperationException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.TransferRepository;
import com.example.virtualwallet.services.contracts.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class TransferServiceImpl implements TransferService {

    public static final String NOT_ENOUGH_BALANCE_ERROR = "Not enough balance in your card";

    public static final String TRANSFER_WITH_EXPIRED_CARD = "You can not make a transfer,because your card is expired";
    public static final String TRANSFER_FUNDS_TO_ANOTHER_USER_S_WALLET = "You can not access and transfer funds to another user's wallet";

    private final TransferRepository transferRepository;

    @Autowired
    public TransferServiceImpl(TransferRepository transferRepository) {
        this.transferRepository = transferRepository;
    }

    @Override
    public List<Transfer> getAllUserTransfers(User user,int userId) {
        if (user.getId()!=userId){
            throw new UnauthorizedOperationException(TRANSFER_FUNDS_TO_ANOTHER_USER_S_WALLET);
        }
        return transferRepository.getAllUserTransfers(userId);
    }

    @Override
    public Transfer getById(int id) {
        return transferRepository.getById(id);
    }

    public HttpStatus isSuccessful() {
        final String uri = "http://localhost:8081/api/withdraw";
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, HttpStatus.class);
    }

    @Override
    public Transfer create(Transfer transfer, User user,int userId) {
        if (!isSuccessful().is2xxSuccessful()) {
            throw new NotEnoughBalanceException(NOT_ENOUGH_BALANCE_ERROR);
        }
        if (transfer.getCard().getExpireDate().before(Date.from(Instant.now()))){
            throw new NotSupportedOperationException(TRANSFER_WITH_EXPIRED_CARD);
        }
        if (user.getId()!=userId){
            throw new UnauthorizedOperationException(TRANSFER_FUNDS_TO_ANOTHER_USER_S_WALLET);
        }
        Wallet currentWallet = transfer.getWallet();
        currentWallet.setBalance((currentWallet.getBalance() + transfer.getAmount()));
        return transferRepository.create(transfer, currentWallet);
    }
}
