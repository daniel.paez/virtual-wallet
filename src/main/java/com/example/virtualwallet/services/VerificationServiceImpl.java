package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Verification;
import com.example.virtualwallet.repositories.contracts.VerificationRepository;
import com.example.virtualwallet.services.contracts.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VerificationServiceImpl implements VerificationService {

    public static final String NEW_VERIFICATION_ACCESS = "You can not make verification for another user";

    public static final String ALL_VERIFICATIONS_ACCESS = "Only admin can access all verifications";

    public static final String VERIFICATION_ACCESS_ERROR = "Only the verification creator or the admin can access it";

    public static final String VERIFICATION_UPDATE_ERROR = "Only the verification creator can update it";

    private final VerificationRepository verificationRepository;

    @Autowired
    public VerificationServiceImpl(VerificationRepository verificationRepository) {
        this.verificationRepository = verificationRepository;
    }

    //users/{userId}/verifications - create
    //users/{userId}/verification - update
    //verifications - getAll - ADMIN
    //verifications/{verificationId} - getById - ADMIN
    //users/{userId}/verification - getById

    @Override
    public Verification create(int userId, Verification verification, User user) {
        if (user.getId()!= userId){
            throw new UnauthorizedOperationException(NEW_VERIFICATION_ACCESS);
        }

        boolean duplicateExists = true;
        try {
            verificationRepository.getById(verification.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Verification", verification.getId());
        }

        return verificationRepository.create(verification);
    }

    @Override
    public Verification update(int userId, Verification verification, User user, int verificationId) {
        if (user.getId()!= userId){
            throw new UnauthorizedOperationException(VERIFICATION_UPDATE_ERROR);
        }

        //proveri statusa dali e podhodqsht za update ili ne
        return verificationRepository.update(verification);
    }

    @Override
    public List<Verification> getAll(User user, int userId) {
        if (!user.getUserRole().getRoleType().equals("admin")) {
            throw new UnauthorizedOperationException(ALL_VERIFICATIONS_ACCESS);
        }
        return verificationRepository.getAll();
    }

    @Override
    public Verification getById(int userId, User user, int verificationId) {
        if (user.getId()!= userId || !user.getUserRole().getRoleType().equals("admin")){
            throw new UnauthorizedOperationException(VERIFICATION_ACCESS_ERROR);
        }
        return verificationRepository.getById(verificationId);
    }
}
