package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {

    public static final String DELETE_ERROR_MESSAGE = "Only the user who registered the wallet can delete it.";
    public static final String TRANSACTION_BALANCE_ERROR_MESSAGE = "There are funds in the wallet you want to delete." +
            "Please transfer them to another wallet first and try again";
    public static final String ALL_WALLETS_ACCESS = "Only the wallets owner can access them";

    public static final String NEW_WALLET_ACCESS = "You can not create a wallet for another user";

    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }


    //users/userId/wallets/walletId

    public void createDefaultWallet(User user) {
        Wallet basicWallet = new Wallet();
        basicWallet.setName("Basic wallet");
        basicWallet.setDefault(true);
        basicWallet.setBalance(0);
        basicWallet.setOwner(user);
        walletRepository.create(basicWallet);
    }

    @Override
    public List<Wallet> getAllUserWallets(User user, int userId) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(ALL_WALLETS_ACCESS);
        }
        return walletRepository.getAllUserWallets(userId);
    }

    @Override
    public Wallet getUserWalletById(User user, int userId, int walletId) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(ALL_WALLETS_ACCESS);
        }
        return walletRepository.getUserWalletById(userId, walletId);
    }

    @Override
    public Wallet getUserWalletByName(User user, int userId, String walletName) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(ALL_WALLETS_ACCESS);
        }
        return walletRepository.getUserWalletByName(userId, walletName);
    }

    @Override
    public Wallet getDefaultUserWallet(int userId) {
        return walletRepository.getDefaultUserWallet(userId);
    }

    @Override
    public Wallet update(int userId, int walletId, Wallet wallet, User user) {
        Wallet walletToUpdate = walletRepository.getUserWalletById(userId, walletId);
        if (walletToUpdate.getName().equals("Basic wallet")) {
            throw new UnauthorizedOperationException("The basic wallet name can not be changed");
        }
        walletToUpdate.setName(wallet.getName());
        walletRepository.update(walletToUpdate);
        return walletToUpdate;
    }

    @Override
    public void makeWalletDefault(int userId, int walletId) {
        Wallet walletToMakeDefault = walletRepository.getById(walletId);
        Wallet currentDefaultWallet = walletRepository.getDefaultUserWallet(userId);
        currentDefaultWallet.setDefault(false);
        walletToMakeDefault.setDefault(true);
        walletRepository.update(currentDefaultWallet);
        walletRepository.update(walletToMakeDefault);
    }


    @Override
    public Wallet create(int userId, Wallet wallet, User user) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(NEW_WALLET_ACCESS);
        }

        boolean duplicateExists = true;
        try {
            walletRepository.getUserWalletByName(userId, wallet.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Wallet", "name", wallet.getName());
        }

        return walletRepository.create(wallet);
    }

    @Override
    public void delete(int userId, int walletId, User user) {
        Wallet walletToDelete = walletRepository.getUserWalletById(userId, walletId);
        if (!walletToDelete.getOwner().equals(user)) {
            throw new UnauthorizedOperationException(DELETE_ERROR_MESSAGE);
        }
        if (walletToDelete.getBalance() != 0) {
            throw new UnauthorizedOperationException(TRANSACTION_BALANCE_ERROR_MESSAGE);
        }

        walletRepository.delete(walletId);
    }

    @Override
    public int getSumFromAllWallets(int userId){
        List<Wallet> allUserWallets = walletRepository.getAllUserWallets(userId);
        int sumFromAll=0;
        for (Wallet allUserWallet : allUserWallets) {
            sumFromAll+=allUserWallet.getBalance();
        }

        return sumFromAll;

    }
}
