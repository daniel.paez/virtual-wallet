package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.services.contracts.UserRoleService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    public static final String USERNAME_CAN_NOT_BE_CHANGED_ONCE_CREATED = "Username can not be changed once created!";
    public static final String MODIFY_USER_BY_ADMIN_ERROR_MESSAGE = "Only admin can change the user role.";
    public static final String BLOCK_UNBLOCK_USER_ERROR_MESSAGE = "Only admin can block or unblock a user.";
    public static final String ACTIVATE_DEACTIVATE_USER_ERROR_MESSAGE = "Only account owner can delete his account";
    private static final String MODIFY_USER_ERROR_MESSAGE = "Only admin or owner can modify a user account.";
    private final UserRepository userRepository;

    private final UserRoleService userRoleService;

    private final PasswordEncoder passwordEncoder;

    private final JavaMailSender mailSender;

    private final WalletService walletService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserRoleService userRoleService, PasswordEncoder passwordEncoder, JavaMailSender mailSender, WalletService walletService) {
        this.userRepository = userRepository;
        this.userRoleService = userRoleService;
        this.passwordEncoder = passwordEncoder;
        this.mailSender = mailSender;
        this.walletService = walletService;
    }


    @Override
    public User getById(int userId, User user) {
        if (user.getUserRole().getRoleType().equals("admin")) {
            return userRepository.getById(userId);
        } else {
            return userRepository.getActiveUserById(userId);
        }
    }

    @Override
    public User getByField(String value, User user) {
        if (user.getUserRole().getRoleType().equals("admin")) {
            return userRepository.getByField(value);
        } else {
            return userRepository.getActiveUserByField(value);
        }
    }

    @Override
    public User getByField(String value) {
        return userRepository.getByField(value);
    }

    @Override
    public User create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.create(user);
        walletService.createDefaultWallet(user);
        return user;

    }

    @Override
    public User update(User userToUpdate, User requester) {
        if (!userToUpdate.getUserRole().getRoleType().equals("admin") && !userToUpdate.getEmail().equals(requester.getEmail())) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        User existingUser = userRepository.getByField(userToUpdate.getEmail());
        try {
            if (existingUser.getId() == userToUpdate.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", userToUpdate.getEmail());
        }


        if (!requester.getUsername().equals(userToUpdate.getUsername())) {
            throw new UnauthorizedOperationException(USERNAME_CAN_NOT_BE_CHANGED_ONCE_CREATED);
        }

        return userRepository.update(userToUpdate);
    }


    @Override
    public User changeUserRole(User user, int userId) {
        if (!user.getUserRole().getRoleType().equals("admin")) {
            throw new UnauthorizedOperationException(MODIFY_USER_BY_ADMIN_ERROR_MESSAGE);
        }
        User userToChangeRole = userRepository.getById(userId);
        if (userToChangeRole.getUserRole().getRoleType().equals("normal")) {
            userToChangeRole.setUserRole(userRoleService.getById(2));
        } else {
            userToChangeRole.setUserRole(userRoleService.getById(1));
        }
        return userRepository.update(userToChangeRole);
    }

    @Override
    public void blockUnblockUser(int userId, User user) {
        if (!user.getUserRole().getRoleType().equals("admin")) {
            throw new UnauthorizedOperationException(BLOCK_UNBLOCK_USER_ERROR_MESSAGE);
        }
        User userToBlockOrUnblock = userRepository.getById(userId);
        userToBlockOrUnblock.setBlocked(!userToBlockOrUnblock.isBlocked());
        userRepository.update(userToBlockOrUnblock);
    }

    @Override
    public void activateDeactivateUser(int userId, User user) {
        User userToActivateOrDeactivate = userRepository.getById(userId);
        if (!user.equals(userToActivateOrDeactivate)) {
            throw new UnauthorizedOperationException(ACTIVATE_DEACTIVATE_USER_ERROR_MESSAGE);
        }
        userToActivateOrDeactivate.setActive(!userToActivateOrDeactivate.isActive());
        userRepository.update(userToActivateOrDeactivate);
    }

    @Override
    public List<User> getAllUsers(User user) {
        if (user.getUserRole().getRoleType().equals("admin")) {
            return userRepository.getAll();
        } else {
            return userRepository.getAllActiveUsers();
        }

    }

    public void register(User user, String siteURL)
            throws UnsupportedEncodingException, MessagingException {
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        String randomCode = RandomString.make(64);
        user.setVerificationCode(randomCode);
        user.setEnabled(false);

        userRepository.create(user);
        walletService.createDefaultWallet(user);

        sendVerificationEmail(user, siteURL);
    }

    private void sendVerificationEmail(User user, String siteURL)
            throws MessagingException, UnsupportedEncodingException {
        String toAddress = user.getEmail();
        String fromAddress = "virtual.wallet.project10@gmail.com";
        String senderName = "Virtual Wallet Project";
        String subject = "Please verify your registration";
        String content = "Dear [[name]],<br>"
                + "Please click the link below to verify your registration:<br>"
                + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>"
                + "Thank you,<br>"
                + "Your virtual wallet team.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", user.getFullName());
        String verifyURL = siteURL + "/auth/verify?code=" + user.getVerificationCode();

        content = content.replace("[[URL]]", verifyURL);

        helper.setText(content, true);

        mailSender.send(message);

        System.out.println("Email has been sent");
    }

    public boolean verify(String verificationCode) {
        User user = userRepository.findByVerificationCode(verificationCode);

        if (user == null || user.isEnabled()) {
            return false;
        } else {
            user.setVerificationCode(null);
            user.setEnabled(true);
            userRepository.update(user);

            return true;
        }
    }



}
