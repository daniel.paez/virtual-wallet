package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.UserRole;

import java.util.List;

public interface UserRoleService {

    List<UserRole> getAll();

    UserRole getById(int id);
}
