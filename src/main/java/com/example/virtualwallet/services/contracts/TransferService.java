package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface TransferService {

    List<Transfer> getAllUserTransfers(User user,int userId);

    Transfer getById(int id);

    Transfer create(Transfer transfer, User user,int userId);
}
