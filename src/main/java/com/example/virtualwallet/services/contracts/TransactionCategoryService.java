package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.TransactionCategory;

import java.util.List;

public interface TransactionCategoryService {

    List<TransactionCategory> getAll();

    TransactionCategory getById(int id);

    List<TransactionCategory> filter(FilterOptions filterOptions);


}
