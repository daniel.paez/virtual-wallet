package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface WalletService {

    void createDefaultWallet(User user);

    List<Wallet> getAllUserWallets(User user, int userId);

    Wallet getUserWalletById(User user, int userId, int walletId);

    Wallet getUserWalletByName(User user, int userId, String walletName);

    Wallet getDefaultUserWallet(int userId);

    Wallet update(int userId, int walletId, Wallet wallet, User user);

    void makeWalletDefault(int userId, int walletId);

    Wallet create(int userId, Wallet wallet, User user);

    void delete(int userId, int walletId, User user);

    public int getSumFromAllWallets(int userId);
}
