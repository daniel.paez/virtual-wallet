package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface TransactionService {

    List<Transaction> filter(FilterOptions filterOptions, User user);

    List<Transaction> filterAllWalletTransactions(FilterOptions filterOptions, User user, int userId, int walletId);

    List<Transaction> filterUserTransactions(FilterOptions filterOptions, User user, int userId);

    List<Transaction> getAll(User user);

    Transaction getById(User user, int id);

    Transaction create(int userId, int walletId, Transaction transaction, User user);

    List<Transaction> getAllWalletTransactions(User user, int userId, int walletId);

    Transaction getWalletTransactionById(User user, int userId, int walletId, int transactionId);

    List<Transaction> getAllUserInternalTransactions(User user,int userId);

    Transaction createInternalTransaction(User user,int userId, Transaction transaction);

    List<Transaction> getAllUserTransactions(int userId);

}
