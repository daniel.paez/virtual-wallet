package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface CardService {

    List<Card> getAllUserCards(User user, int userId);

    Card getUserCardById(User user, int userId, int cardId);
    Card getCardByNumber(User user,String cardNumber);

    Card create(int userId, Card card, User user);

    void delete(int userId, int cardId, User user);

}
