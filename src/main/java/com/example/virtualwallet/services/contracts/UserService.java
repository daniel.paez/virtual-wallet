package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.User;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface UserService {

    User getById(int userId, User user);

    User getByField(String value, User user);

    User getByField(String value);

    User create(User user);

    User update(User user, User requester);

//    void delete(int id, User requester);

    User changeUserRole(User admin, int userId);

    void blockUnblockUser(int userId, User user);

    void activateDeactivateUser(int userId, User user);


    List<User> getAllUsers(User user);

    void register(User user, String siteURL) throws UnsupportedEncodingException, MessagingException;

    boolean verify(String verificationCode);
}
