package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Verification;
import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface VerificationService {
    Verification create (int userId, Verification verification, User user);

    Verification update (int userId,Verification verification, User user, int verificationId);

    List<Verification> getAll (User user,int userId);

    Verification getById(int userId, User user, int verificationId);
}
