package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.NotSupportedOperationException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import com.example.virtualwallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {


    public static final String ALL_CARDS_ACCESS = "Only the cards owner can access them";

    public static final String NEW_CARD_ACCESS = "You can not create a card for another user";
    public static final String DELETE_ERROR_MESSAGE = "Only the user who registered the card can delete it.";
    public static final String REGISTER_AN_EXPIRED_CARD = "You can not register an expired card";
    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    //users/{userId}/cards/{cardId}
    @Override
    public List<Card> getAllUserCards(User user, int userId) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(ALL_CARDS_ACCESS);
        }
        return cardRepository.getAllUserCards(userId);
    }

    @Override
    public Card getUserCardById(User user, int userId, int cardId) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(ALL_CARDS_ACCESS);
        }
        return cardRepository.getUserCardById(userId, cardId);
    }

    @Override
    public Card getCardByNumber(User user, String cardNumber) {
        Card card = cardRepository.getByField("card_number",cardNumber);
        if (card.getOwner().getId()!=user.getId()){
            throw new UnauthorizedOperationException(ALL_CARDS_ACCESS);
        }
        return card;
    }


    @Override
    public Card create(int userId, Card card, User user) {
        if (user.getId() != userId) {
            throw new UnauthorizedOperationException(NEW_CARD_ACCESS);
        }
        boolean duplicateExists = true;
        try {
            cardRepository.getByField("cardNumber", card.getCardNumber());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Card", "number", card.getCardNumber());
        }

        if (card.getExpireDate().before(Date.from(Instant.now()))){
            throw new NotSupportedOperationException(REGISTER_AN_EXPIRED_CARD);
        }
        return cardRepository.create(card);
    }

    @Override
    public void delete(int userId, int cardId, User user) {
        Card cardToDelete = cardRepository.getUserCardById(userId, cardId);
        if (!cardToDelete.getOwner().equals(user)) {
            throw new UnauthorizedOperationException(DELETE_ERROR_MESSAGE);
        }
        cardRepository.delete(cardId);
    }
}
