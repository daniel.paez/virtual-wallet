package com.example.virtualwallet.services;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.TransactionCategory;
import com.example.virtualwallet.repositories.contracts.TransactionCategoryRepository;
import com.example.virtualwallet.services.contracts.TransactionCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionCategoryServiceImpl implements TransactionCategoryService {


    private final TransactionCategoryRepository transactionCategoryRepository;

    @Autowired
    public TransactionCategoryServiceImpl(TransactionCategoryRepository transactionCategoryRepository) {
        this.transactionCategoryRepository = transactionCategoryRepository;
    }


    @Override
    public List<TransactionCategory> getAll() {
        return transactionCategoryRepository.getAll();
    }

    @Override
    public TransactionCategory getById(int id) {
        return transactionCategoryRepository.getById(id);
    }

    @Override
    public List<TransactionCategory> filter(FilterOptions filterOptions) {
        //TODO:implement later- filter by direction(sender, recipient)
        return null;
    }
}
