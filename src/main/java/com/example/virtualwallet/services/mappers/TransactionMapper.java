package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.*;
import com.example.virtualwallet.services.contracts.TransactionCategoryService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;

@Component
public class TransactionMapper {


    private final TransactionCategoryService transactionCategoryService;

    private final WalletService walletService;
    private final UserService userService;

    private final UserMapper userMapper;


    public TransactionMapper(TransactionCategoryService transactionCategoryService, WalletService walletService, UserService userService, UserMapper userMapper) {
        this.transactionCategoryService = transactionCategoryService;
        this.walletService = walletService;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public Transaction fromDto(TransactionDto transactionDto, User user) {
        Transaction transaction = new Transaction();
        dtoToObject(transactionDto, transaction, user);
        return transaction;
    }

    private void dtoToObject(TransactionDto transactionDto, Transaction transaction,
                             User user) {
        Wallet senderWallet = walletService.getUserWalletByName(user, user.getId(), transactionDto.getWalletName());
        Wallet recipientWallet = walletService.getDefaultUserWallet(userService.getByField(transactionDto.getRecipientInfo()).getId());

        transaction.setTransactionDate(Date.from(Instant.now()));
        transaction.setCategory(transactionCategoryService.getById(transactionDto.getCategoryId()));
        transaction.setAmount(transactionDto.getAmount());
        transaction.setSenderWallet(senderWallet);
        transaction.setRecipientWallet(recipientWallet);


    }

    public TransactionDtoOut objectToDto(Transaction transaction) {
        TransactionDtoOut transactionDtoOut = new TransactionDtoOut();
        UserDtoOut userDtoOutSender = userMapper.objectToDto(transaction.getSenderWallet().getOwner());
        UserDtoOut userDtoOutRecipient = userMapper.objectToDto(transaction.getRecipientWallet().getOwner());
        transactionDtoOut.setDate(transaction.getTransactionDate());
        transactionDtoOut.setAmount(transaction.getAmount());
        transactionDtoOut.setSender(userDtoOutSender);
        transactionDtoOut.setRecipient(userDtoOutRecipient);
        return transactionDtoOut;

    }

    public Transaction fromInternalTransaction(User user,InternalTransactionDto internalTransactionDto){
        int userId = user.getId();
        Transaction transaction = new Transaction();
        transaction.setRecipientWallet(walletService.getUserWalletById(user,userId,internalTransactionDto.getIncomingWalletId()));
        transaction.setSenderWallet(walletService.getUserWalletById(user,userId,internalTransactionDto.getOutgoingWalletId()));
        transaction.setCategory(transactionCategoryService.getById(internalTransactionDto.getCategoryId()));
        transaction.setTransactionDate(Date.from(Instant.now()));
        transaction.setAmount(internalTransactionDto.getAmount());
        return transaction;
    }

    public InternalTransactionDtoOut objectToInternalTransactionDtoOut(Transaction transaction){
        InternalTransactionDtoOut internalTransactionDtoOut = new InternalTransactionDtoOut();
        internalTransactionDtoOut.setAmount(transaction.getAmount());
        internalTransactionDtoOut.setIncomingWalletName(transaction.getRecipientWallet().getName());
        internalTransactionDtoOut.setOutgoingWalletName(transaction.getSenderWallet().getName());
        internalTransactionDtoOut.setDate(transaction.getTransactionDate());
        return internalTransactionDtoOut;
    }
}
