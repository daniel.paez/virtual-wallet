package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Verification;
import com.example.virtualwallet.models.VerificationStatus;
import com.example.virtualwallet.models.dtos.VerificationDto;
import com.example.virtualwallet.services.contracts.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.Instant;


@Component
public class VerificationMapper {

    private final VerificationService verificationService;


    @Autowired
    public VerificationMapper(VerificationService verificationService) {
        this.verificationService = verificationService;
    }

    public Verification fromDto(VerificationDto verificationDto, User user) {
        Verification verification = new Verification();
        dtoToObject(user, verificationDto, verification);
        return verification;
    }

    public Verification fromDto(User user, VerificationDto verificationDto, int verificationId) {
        Verification verification = verificationService.getById(user.getId(), user, verificationId);
        dtoToObject(user, verificationDto, verification);
        return verification;
    }

    private void dtoToObject(User user, VerificationDto verificationDto, Verification verification) {
        //TODO: to be implemented
        verification.setVerificationRequestDate(Date.from(Instant.now()));
        // TODO: with enum
        //verification.setStatus(VerificationStatus);
        verification.setUser(user);
//        verification.setIdCardPhoto(verificationDto.getIdCardPhoto());
//        verification.setSelfie(verificationDto.getSelfie());
    }
}
