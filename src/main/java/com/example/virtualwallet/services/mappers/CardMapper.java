package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

@Component
public class CardMapper {

    private final CardService cardService;

    @Autowired
    public CardMapper(CardService cardService) {
        this.cardService = cardService;
    }


    public Card fromDto(CardDto cardDto, User user) throws ParseException {
        Card card = new Card();
        dtoToObject(user, cardDto, card);
        return card;
    }

    public Card fromDto(User user, CardDto cardDto, int cardId) throws ParseException {
        Card card = cardService.getUserCardById(user, user.getId(), cardId);
        dtoToObject(user, cardDto, card);
        return card;
    }

    private void dtoToObject(User user, CardDto cardDto, Card card) throws ParseException {
        card.setCardHolder(cardDto.getCardHolder());
        card.setCardNumber(cardDto.getCardNumber());
        card.setCheckNumber(cardDto.getCheckNumber());

        String carDtoExpireDate = cardDto.getExpireDate(); // for example
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
        simpleDateFormat.setLenient(false);
        Date expiry = simpleDateFormat.parse(carDtoExpireDate);

        card.setExpireDate(expiry);
        card.setCardType(cardDto.getCardType());
        card.setOwner(user);
    }
}
