package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.TransferDto;
import com.example.virtualwallet.models.dtos.TransferDtoOut;
import com.example.virtualwallet.services.contracts.CardService;
import com.example.virtualwallet.services.contracts.TransferService;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;

@Component
public class TransferMapper {

    private final TransferService transferService;

    private final CardService cardService;

    private final WalletService walletService;


    @Autowired
    public TransferMapper(TransferService transferService, CardService cardService, WalletService walletService) {
        this.transferService = transferService;
        this.cardService = cardService;
        this.walletService = walletService;
    }

    public Transfer fromDto(TransferDto transferDto, User user) {
        Transfer transfer = new Transfer();
        dtoToObject(transferDto, transfer, user);
        return transfer;
    }

    private void dtoToObject(TransferDto transferDto, Transfer transfer, User user) {
        transfer.setTransferDate(Date.from(Instant.now()));
        transfer.setAmount(transferDto.getAmount());
        Card card = cardService.getCardByNumber(user,transferDto.getCardNumber());
        Wallet wallet = walletService.getUserWalletByName(user, user.getId(), transferDto.getWalletName());
        transfer.setCard(card);
        transfer.setWallet(wallet);
    }

    public TransferDtoOut objectToDto(Transfer transfer) {
        TransferDtoOut transferDtoOut = new TransferDtoOut();
        String holderName = transfer.getCard().getCardHolder();
        transferDtoOut.setAmount(transfer.getAmount());
        transferDtoOut.setCardHolder(holderName);
        transferDtoOut.setTransferDate(transfer.getTransferDate());
        transferDtoOut.setWalletBalance(transfer.getWallet().getBalance());
        transferDtoOut.setCardNumber(transfer.getCard().getCardNumber());
        return transferDtoOut;
    }
}
