package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.UserRole;
import com.example.virtualwallet.models.dtos.ProfileDto;
import com.example.virtualwallet.models.dtos.RegisterDto;
import com.example.virtualwallet.models.dtos.UserDto;
import com.example.virtualwallet.models.dtos.UserDtoOut;
import com.example.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.Date;

import static java.nio.file.Files.readString;

@Component
public class UserMapper {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public User fromDto(UserDto userDto) throws IOException {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDTO, int id, User requester) throws IOException {
        User user = userService.getById(id, requester);
        dtoToObject(userDTO, user);;
        return user;
    }

    public void dtoToObject(UserDto userDto, User user) throws IOException {
        UserRole userRole = new UserRole();
        userRole.setId(1);
        userRole.setRoleType("normal");

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
//        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setPassword(userDto.getPassword());
        user.setUsername(userDto.getUsername());

        user.setPhoto("default.png");

        user.setUserRole(userRole);
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setRegistrationDate(Date.from(Instant.now()));
    }


    public UserDtoOut objectToDto(User user) {
        UserDtoOut userDtoOut = new UserDtoOut();
        userDtoOut.setFirstName(user.getFirstName());
        userDtoOut.setLastName(user.getLastName());
        userDtoOut.setUsername(user.getUsername());
        return userDtoOut;
    }

    //TODO: fix methods below

    public User fromDto(RegisterDto registerDto) throws IOException {
        UserDto userDto = registerDtoToUserDto(registerDto);
        User user = new User();
        dtoToObject(userDto, user);
//        user.setPhoto("default.png");
        return user;
    }

    public UserDto registerDtoToUserDto(RegisterDto registerDto) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(registerDto.getFirstName());
        userDto.setLastName(registerDto.getLastName());
        userDto.setUsername(registerDto.getUsername());
        userDto.setEmail(registerDto.getEmail());
        userDto.setPassword(registerDto.getPassword());
        userDto.setPhoneNumber(registerDto.getPhoneNumber());
//        userDto.setPhoto(registerDto.getPhoto());
        return userDto;
    }

    //update profile
    public User fromProfileDto(ProfileDto profileDto) throws IOException {
        User user = userService.getByField(profileDto.getEmail());
        user.setFirstName(profileDto.getFirstName());
        user.setLastName(profileDto.getLastName());
        user.setEmail(profileDto.getEmail());
        user.setPassword(passwordEncoder.encode(profileDto.getPassword()));
        user.setPhoneNumber(profileDto.getPhoneNumber());


        //TODO: tuka se updatva snimkata

//        if (!profileDto.getPhoto().isEmpty()) {
//            String Path_Directory = new ClassPathResource("static/image/").getFile().getAbsolutePath();
//            Files.copy(profileDto.getPhoto().getInputStream(),
//                    Paths.get(Path_Directory + File.separator + profileDto.getPhoto().getOriginalFilename()),
//                    StandardCopyOption.REPLACE_EXISTING);
//
//            user.setPhoto(profileDto.getPhoto().getOriginalFilename());
//        }
//

//        user.setPhoto(profileDto.getPhoto().getOriginalFilename());

        return user;
    }

    public ProfileDto toProfileDto(User user) throws IOException {
        ProfileDto profileDto = new ProfileDto();
        profileDto.setEmail(user.getEmail());
        profileDto.setPassword(user.getPassword());
        profileDto.setPasswordConfirm(user.getPassword());
        //TODO: check if photo functionality works
        profileDto.setPhoneNumber(user.getPhoneNumber());
        profileDto.setFirstName(user.getFirstName());
        profileDto.setLastName(user.getLastName());

//        if (!profileDto.getPhoto().isEmpty()) {
//            String Path_Directory = new ClassPathResource("static/image/").getFile().getPath();
//            Files.copy(profileDto.getPhoto().getInputStream(),
//                    Paths.get(Path_Directory + File.separator + profileDto.getPhoto().getOriginalFilename()),
//                    StandardCopyOption.REPLACE_EXISTING);
//
////            profileDto.setPhoto(user.getPhoto());
//        }

        Path path = Paths.get("src/main/resources/static" + user.getPhoto());

        byte[] content = Files.readAllBytes(path);

        String name = path.getFileName().toString();

        String originalFileName = path.getFileName().toString();

        String contentType = "image/png";

        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);

        profileDto.setPhoto(result);

        return profileDto;
    }
}
