package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.dtos.FilterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FilterMapper {


    public FilterOptions fromDto(FilterDto filterDto) {

        FilterOptions filterOptions = new FilterOptions(
                Optional.ofNullable(filterDto.getStartDate()),
                Optional.ofNullable(filterDto.getEndDate()),
                Optional.ofNullable(filterDto.getSenderUsername()),
                Optional.ofNullable(filterDto.getRecipientUsername()),
                Optional.ofNullable(filterDto.getDirection()),
                Optional.ofNullable(filterDto.getSortBy()),
                Optional.ofNullable(filterDto.getSortOrder())
        );

        return filterOptions;
    }
}
