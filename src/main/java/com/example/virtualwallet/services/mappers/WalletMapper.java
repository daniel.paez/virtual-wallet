package com.example.virtualwallet.services.mappers;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.WalletDto;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class WalletMapper {

    private final WalletService walletService;

    @Autowired
    public WalletMapper(WalletService walletService) {
        this.walletService = walletService;
    }


    public Wallet fromDto(WalletDto walletDto, User user) {
        Wallet wallet = new Wallet();
        dtoToObject(user, walletDto, wallet);
        return wallet;
    }

    public Wallet fromDto(User user, WalletDto walletDto, int walletId) {
        Wallet wallet = walletService.getUserWalletById(user, user.getId(), walletId);
        dtoToObject(user, walletDto, wallet);
        return wallet;
    }

    private void dtoToObject(User user, WalletDto walletDto, Wallet wallet) {
        wallet.setName(walletDto.getName());
        wallet.setBalance(0);
        wallet.setDefault(false);
        wallet.setOwner(user);
    }

}
