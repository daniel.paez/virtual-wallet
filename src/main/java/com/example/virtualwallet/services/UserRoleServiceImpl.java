package com.example.virtualwallet.services;

import com.example.virtualwallet.models.UserRole;
import com.example.virtualwallet.repositories.contracts.UserRoleRepository;
import com.example.virtualwallet.services.contracts.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public List<UserRole> getAll() {
        return userRoleRepository.getAll();
    }

    @Override
    public UserRole getById(int id) {
        return userRoleRepository.getById(id);
    }
}
