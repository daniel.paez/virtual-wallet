package com.example.virtualwallet.exceptions;

public class NotSupportedOperationException extends RuntimeException {
    public NotSupportedOperationException(String message) {
        super(message);
    }
}
