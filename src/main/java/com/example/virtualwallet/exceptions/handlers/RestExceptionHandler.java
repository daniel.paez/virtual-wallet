package com.example.virtualwallet.exceptions.handlers;

import com.example.virtualwallet.exceptions.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiError apiError =
                new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return handleExceptionInternal(
                ex, apiError, headers, apiError.getStatus(), request);
    }

    private ResponseEntity<Object> getObjectResponseEntity(JSONObject errorJson, HttpStatus status, String message) {
        try {
            errorJson.put("status", status.value());
            errorJson.put("error", status.getReasonPhrase());
            errorJson.put("message", message.split(":")[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(errorJson.toString(), status);
    }


    @ExceptionHandler(DuplicateEntityException.class)
    public ResponseEntity<Object> handleDuplicateEntity(DuplicateEntityException duplicateEntityException
    ) {
        JSONObject errorJson = new JSONObject();
        HttpStatus status = HttpStatus.CONFLICT;
        return getObjectResponseEntity(errorJson, status, duplicateEntityException.getMessage());
    }


    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException entityNotFoundException) {
        JSONObject errorJson = new JSONObject();
        HttpStatus status = HttpStatus.NOT_FOUND;
        return getObjectResponseEntity(errorJson, status, entityNotFoundException.getMessage());
    }

    @ExceptionHandler(NotEnoughBalanceException.class)
    public ResponseEntity<Object> handleNotEnoughBalance(NotEnoughBalanceException notEnoughBalanceException) {
        JSONObject errorJson = new JSONObject();
        HttpStatus status = HttpStatus.NOT_ACCEPTABLE;
        return getObjectResponseEntity(errorJson, status, notEnoughBalanceException.getMessage());
    }

    @ExceptionHandler(UnauthorizedOperationException.class)
    public ResponseEntity<Object> handleUnauthorizedOperation(UnauthorizedOperationException unauthorizedOperationException) {
        JSONObject errorJson = new JSONObject();
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return getObjectResponseEntity(errorJson, status, unauthorizedOperationException.getMessage());
    }

    @ExceptionHandler(NotSupportedOperationException.class)
    public ResponseEntity<Object> handleNotSupportedOperation(NotSupportedOperationException notSupportedOperationException) {
        JSONObject errorJson = new JSONObject();
        HttpStatus status = HttpStatus.CONFLICT;
        return getObjectResponseEntity(errorJson, status, notSupportedOperationException.getMessage());
    }


}
