package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.*;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.mappers.TransactionMapper;
import com.example.virtualwallet.services.mappers.UserMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    private final TransactionService transactionService;

    private final TransactionMapper transactionMapper;

    public UserController(UserService userService,
                          UserMapper userMapper,
                          AuthenticationHelper authenticationHelper, TransactionService transactionService, TransactionMapper transactionMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        User requester = authenticationHelper.tryGetUser(headers);
        return userService.getAllUsers(requester);
    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User requester = authenticationHelper.tryGetUser(headers);
        return userService.getById(id, requester);
    }

    @PostMapping()
    public User create(@Valid @RequestBody UserDto userDto) throws IOException {
        User user = userMapper.fromDto(userDto);
        userService.create(user);
        return user;
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDto userDto) throws IOException {
        User requester = authenticationHelper.tryGetUser(headers);
        User user = userMapper.fromDto(userDto, id, requester);
        userService.update(user, requester);
        return user;
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User requester = authenticationHelper.tryGetUser(headers);
        userService.activateDeactivateUser(id, requester);
    }

    //TODO: methods for get all user transactions from all wallets

    @GetMapping("/{userId}/transactions/filter")
    public List<TransactionDtoOut> filterAllUserTransactions(@RequestHeader HttpHeaders headers, @PathVariable int userId,

                                                             @RequestParam(required = false) Optional<String> startDate,
                                                             @RequestParam(required = false) Optional<String> endDate,
                                                             @RequestParam(required = false) Optional<String> senderUsername,
                                                             @RequestParam(required = false) Optional<String> recipientUsername,
                                                             @RequestParam(required = false) Optional<String> direction,
                                                             @RequestParam(required = false) Optional<String> sortBy,
                                                             @RequestParam(required = false) Optional<String> sortOrder) {
        User user = authenticationHelper.tryGetUser(headers);

        return transactionService.filterUserTransactions(new FilterOptions(startDate, endDate, senderUsername, recipientUsername,
                        direction, sortBy, sortOrder), user, userId)
                .stream().map(transactionMapper::objectToDto).collect(Collectors.toList());

    }

    @GetMapping("/{userId}/internal-transactions")
    List<InternalTransactionDtoOut> getAllUsersInternalTransactions(@RequestHeader HttpHeaders headers, @PathVariable int userId){
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getAllUserInternalTransactions(user,userId).stream()
                .map(transactionMapper::objectToInternalTransactionDtoOut)
                .collect(Collectors.toList());

    }

    @PostMapping("/{userId}/internal-transactions")
    InternalTransactionDtoOut createInternalTransaction(@RequestHeader HttpHeaders headers,@PathVariable int userId,@RequestBody @Valid InternalTransactionDto internalTransactionDto){
        User user = authenticationHelper.tryGetUser(headers);
        Transaction transaction = transactionMapper.fromInternalTransaction(user,internalTransactionDto);
        transactionService.createInternalTransaction(user,userId,transaction);
        return transactionMapper.objectToInternalTransactionDtoOut(transaction);
    }

}
