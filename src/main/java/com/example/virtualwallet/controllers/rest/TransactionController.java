package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDtoOut;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.mappers.TransactionMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/transactions")
public class TransactionController {

    private final TransactionService transactionService;
    private final AuthenticationHelper authenticationHelper;
    private final TransactionMapper transactionMapper;

    public TransactionController(TransactionService transactionService,
                                 AuthenticationHelper authenticationHelper,
                                 TransactionMapper transactionMapper) {
        this.transactionService = transactionService;
        this.authenticationHelper = authenticationHelper;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping()
    public List<TransactionDtoOut> filterForAdmin(@RequestHeader HttpHeaders headers,
                                                  @RequestParam(required = false) Optional<String> startDate,
                                                  @RequestParam(required = false) Optional<String> endDate,
                                                  @RequestParam(required = false) Optional<String> senderUsername,
                                                  @RequestParam(required = false) Optional<String> recipientUsername,
                                                  @RequestParam(required = false) Optional<String> direction,
                                                  @RequestParam(required = false) Optional<String> sortBy,
                                                  @RequestParam(required = false) Optional<String> sortOrder) {
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.filter(new FilterOptions(startDate, endDate, senderUsername, recipientUsername,
                        direction, sortBy, sortOrder), user)
                .stream().map(transactionMapper::objectToDto).collect(Collectors.toList());
    }


}
