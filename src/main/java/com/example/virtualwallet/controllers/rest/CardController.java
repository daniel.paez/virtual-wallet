package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.services.contracts.CardService;
import com.example.virtualwallet.services.mappers.CardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api/users/{userId}/cards")
public class CardController {
    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardController(CardService cardService,
                          CardMapper cardMapper,
                          AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Card> getAllUserCards(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        User user = authenticationHelper.tryGetUser(headers);
        return cardService.getAllUserCards(user, userId);
    }

    @GetMapping("/{cardId}")
    public Card getUserCardById(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int cardId) {
        User user = authenticationHelper.tryGetUser(headers);
        return cardService.getUserCardById(user, userId, cardId);
    }

    @PostMapping()
    public Card create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CardDto cardDto, @PathVariable int userId) throws ParseException {
        User user = authenticationHelper.tryGetUser(headers);
        Card card = cardMapper.fromDto(cardDto, user);
        cardService.create(userId, card, user);
        return card;
    }

    @DeleteMapping("/{cardId}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int cardId) {
        User user = authenticationHelper.tryGetUser(headers);
        cardService.delete(userId, cardId, user);
    }
}
