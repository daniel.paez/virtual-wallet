package com.example.virtualwallet.controllers.rest;


import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.models.dtos.TransactionDtoOut;
import com.example.virtualwallet.models.dtos.WalletDto;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.contracts.WalletService;
import com.example.virtualwallet.services.mappers.TransactionMapper;
import com.example.virtualwallet.services.mappers.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/users/{userId}/wallets")
public class WalletController {

    private final WalletService walletService;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper authenticationHelper;

    private final WalletMapper walletMapper;


    @Autowired
    public WalletController(WalletService walletService,
                            TransactionService transactionService,
                            TransactionMapper transactionMapper,
                            AuthenticationHelper authenticationHelper, WalletMapper walletMapper) {
        this.walletService = walletService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
        this.walletMapper = walletMapper;
    }

    @GetMapping
    public List<Wallet> getAllUserWallets(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        User user = authenticationHelper.tryGetUser(headers);
        return walletService.getAllUserWallets(user, userId);
    }

    @GetMapping("/{walletId}")
    public Wallet getUserWalletById(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int walletId) {
        User user = authenticationHelper.tryGetUser(headers);

        return walletService.getUserWalletById(user, userId, walletId);
    }

    @PostMapping()
    public Wallet create(@RequestHeader HttpHeaders headers, @PathVariable int userId, @RequestBody @Valid WalletDto walletDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Wallet wallet = walletMapper.fromDto(walletDto, user);
        walletService.create(userId, wallet, user);
        return wallet;
    }

    @DeleteMapping("/{walletId}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int userId, @PathVariable int walletId) {
        User user = authenticationHelper.tryGetUser(headers);
        walletService.delete(userId, walletId, user);
    }

    //Get - list all transactions for wallet
    //users/{userId}/wallets/{walletId}/transactions

    //Get - get specific transaction
    //users/{userId}/wallets/{walletId}/transactions/{transactionId}

    //Post - post new transaction
    //users/{userId}/wallets/{walletId}/transactions

    //Filter transaction
    //users/{userId}/wallets/{walletId}/transactions

    @GetMapping("/{walletId}/transactions/filter")
    public List<TransactionDtoOut> filterAllWalletTransactions(@RequestHeader HttpHeaders headers,
                                                               @PathVariable int userId,
                                                               @PathVariable int walletId,
                                                               @RequestParam(required = false) Optional<String> startDate,
                                                               @RequestParam(required = false) Optional<String> endDate,
                                                               @RequestParam(required = false) Optional<String> senderUsername,
                                                               @RequestParam(required = false) Optional<String> recipientUsername,
                                                               @RequestParam(required = false) Optional<String> direction,
                                                               @RequestParam(required = false) Optional<String> sortBy,
                                                               @RequestParam(required = false) Optional<String> sortOrder) {

        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.filterAllWalletTransactions(new FilterOptions(startDate, endDate, senderUsername, recipientUsername,
                        direction, sortBy, sortOrder), user, userId, walletId)
                .stream().map(transactionMapper::objectToDto).collect(Collectors.toList());
    }

    @GetMapping("/{walletId}/transactions")
    private List<TransactionDtoOut> getAllWalletTransactions(@RequestHeader HttpHeaders headers,
                                                             @PathVariable int userId,
                                                             @PathVariable int walletId
    ) {
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getAllWalletTransactions(user, userId, walletId).stream()
                .map(transactionMapper::objectToDto).collect(Collectors.toList());
    }

    @GetMapping("/{walletId}/transactions/{transactionId}")
    private TransactionDtoOut getWalletTransactionById(@RequestHeader HttpHeaders headers,
                                                       @PathVariable int userId,
                                                       @PathVariable int walletId,
                                                       @PathVariable int transactionId) {
        User user = authenticationHelper.tryGetUser(headers);
        Transaction transaction = transactionService.getWalletTransactionById(user, userId, walletId, transactionId);
        return transactionMapper.objectToDto(transaction);

    }

    @PostMapping("/{walletId}/transactions")
    private TransactionDtoOut create(@RequestHeader HttpHeaders headers, @PathVariable int userId,
                                     @PathVariable int walletId,
                                     @Valid @RequestBody TransactionDto transactionDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Transaction transaction = transactionMapper.fromDto(transactionDto, user);
        transactionService.create(userId, walletId, transaction, user);
        return transactionMapper.objectToDto(transaction);
    }


}
