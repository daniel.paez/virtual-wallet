package com.example.virtualwallet.controllers.rest;


import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDtoOut;
import com.example.virtualwallet.models.dtos.TransferDto;
import com.example.virtualwallet.models.dtos.TransferDtoOut;
import com.example.virtualwallet.services.contracts.TransferService;
import com.example.virtualwallet.services.mappers.TransferMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/users/{userId}/transfers")
public class TransferController {
    private final TransferService transferService;
    private final AuthenticationHelper authenticationHelper;
    private final TransferMapper transferMapper;

    @Autowired
    public TransferController(TransferService transferService,
                              AuthenticationHelper authenticationHelper,
                              TransferMapper transferMapper) {
        this.transferService = transferService;
        this.authenticationHelper = authenticationHelper;
        this.transferMapper = transferMapper;
    }


    @PostMapping
    public TransferDtoOut create(@RequestHeader HttpHeaders headers,
                                 @PathVariable int userId,
                                 @Valid @RequestBody TransferDto transferDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Transfer transfer = transferMapper.fromDto(transferDto, user);
        transferService.create(transfer, user,userId);
        return transferMapper.objectToDto(transfer);
    }

    @GetMapping()
    private List<TransferDtoOut> getAllUserTransfers(@RequestHeader HttpHeaders headers,
                                                             @PathVariable int userId

    ) {
        User user = authenticationHelper.tryGetUser(headers);
        return transferService.getAllUserTransfers(user,userId).stream().map(transferMapper::objectToDto)
                .collect(Collectors.toList());
    }
}
