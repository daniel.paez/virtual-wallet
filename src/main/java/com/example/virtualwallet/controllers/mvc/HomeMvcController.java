package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;


    public HomeMvcController(AuthenticationHelper authenticationHelper) {
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public int getCurrentUserId(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getId();
        }
        return 0;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @GetMapping
    public String showHomePage(){
        return "index";
    }

    //TODO: about and team to be implemented
    @GetMapping("/about")
    public String showAboutPage(){
        return "about-us";
    }

    @GetMapping("/team")
    public String showTeamPage(){
        return "team";
    }

}
