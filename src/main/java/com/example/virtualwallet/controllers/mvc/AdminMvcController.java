package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.*;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.mappers.TransactionMapper;
import com.example.virtualwallet.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin-portal")
public class AdminMvcController {

    private final UserService userService;

    private final TransactionService transactionService;

    private final UserMapper userMapper;

    private final TransactionMapper transactionMapper;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AdminMvcController(UserService userService,
                              TransactionService transactionService,
                              UserMapper userMapper,
                              TransactionMapper transactionMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.userMapper = userMapper;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }


    @ModelAttribute("transactions")
    public List<Transaction> getAll(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return transactionService.getAll(user);
    }

    @GetMapping("/transactions")
    public String showTransactions(@ModelAttribute("filterOptions") FilterOptions filterOptions,
                                   @ModelAttribute("userData") String string,
                                   Model model, HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        if (!user.getUserRole().getRoleType().equals("admin")) {
            return "access-denied";
        }

        model.addAttribute("filterOptions", filterOptions);
        model.addAttribute("transactions", transactionService.getAll(user));
        return "admin-portal-transactions";
    }

    @PostMapping("/transactions")
    public String showTransactions(@ModelAttribute("filterOptions") FilterOptions filterOptions,
//                               @ModelAttribute("userData") String info,
                               Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.getUserRole().getRoleType().equals("admin")) {
            return "access-denied";
        }
//        model.addAttribute("userData", userService.getByField(info, user));
        model.addAttribute("filterOptions", filterOptions);
        model.addAttribute("transactions", transactionService.filter(filterOptions, user));
        return "admin-portal-transactions";
    }

    @GetMapping("/users")
    public String showAllUsers(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.getUserRole().getRoleType().equals("admin")) {
            return "access-denied";
        }
//        model.addAttribute("filterOptions", filterOptions);
        model.addAttribute("userData", new UserDataFilter());
        model.addAttribute("users", userService.getAllUsers(user));
        return "admin-portal-users";
    }

    @PostMapping("/users")
    public String showAllUsers(@ModelAttribute("userData") UserDataFilter userDataFilter,
                               Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.getUserRole().getRoleType().equals("admin")) {
            return "access-denied";
        }
        List<User> singleUser = new ArrayList<>();
        singleUser.add(userService.getByField(userDataFilter.getInfo(), user));

        if (userDataFilter.getInfo().isBlank()) {
            model.addAttribute("users", userService.getAllUsers(user));
        } else {
            model.addAttribute("users", singleUser);
        }

        return "admin-portal-users";
    }

    @PostMapping("/users/{userId}/block-unblock")
    public String blockUnblockUser(@PathVariable int userId, Model model, HttpSession session) {
        User admin = authenticationHelper.tryGetUser(session);
        try {
            userService.blockUnblockUser(userId, admin);
            return "redirect:/admin-portal/users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/users/{userId}/change-role")
    public String changeUserRole(@PathVariable int userId, Model model, HttpSession session) {
        User admin = authenticationHelper.tryGetUser(session);
        try {
            userService.changeUserRole(admin, userId);
            return "redirect:/admin-portal/users";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
