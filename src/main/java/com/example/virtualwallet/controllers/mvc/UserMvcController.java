package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.ProfileDto;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import com.example.virtualwallet.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;

    private final WalletService walletService;

    private final AuthenticationHelper authenticationHelper;

    private final UserMapper userMapper;


    @Autowired
    public UserMvcController(UserService userService, WalletService walletService, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.userService = userService;
        this.walletService = walletService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }


    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @ModelAttribute("currentUserId")
    public int getCurrentUserId(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getId();
        }
        return 0;
    }


    @GetMapping("/{userId}/profile")
    public String showProfilePage(Model model,HttpSession session,@PathVariable int userId) throws IOException{
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            ProfileDto profileDto = userMapper.toProfileDto(user);
            model.addAttribute("profile", profileDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        return "profile";
    }

    @GetMapping("/{userId}/profile/image")
    public String showProfileImage(HttpSession session,@PathVariable int userId){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "photo";
    }

    @PostMapping("/{userId}/profile/image")
    public String updateProfilePhoto (@RequestParam("multiPhoto") MultipartFile multipartFile,
                                      HttpSession session, Model model,@PathVariable int userId) throws IOException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        Path initialPath = Paths.get(multipartFile.getOriginalFilename());

        Path path = Paths.get("src/main/resources/static/image/" + user.getId() + initialPath);

        Files.write(path, multipartFile.getBytes());

        String pathDb = "/image/" + user.getId() + multipartFile.getOriginalFilename();

        user.setPhoto(pathDb);

        userService.update(user, user);

        ProfileDto profileDto = userMapper.toProfileDto(user);
        model.addAttribute("profile", profileDto);

        return "redirect:/users/{userId}/profile";
    }

    @PostMapping("/{userId}/profile/update")
    public String updateProfile (@Valid @ModelAttribute("profile") ProfileDto profile,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session,
                                 @PathVariable int userId) throws IOException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/ath/login";
        }


        if (errors.hasErrors()) {
            return "profile";
        }

        if (!profile.getPassword().equals(profile.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return  "profile";
        }

        try {
            User userToUpdate = userMapper.fromProfileDto(profile);
            userService.update(userToUpdate, user);

            return "redirect:/users/{userId}/profile";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_user", e.getMessage());
            return "profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @ModelAttribute("allBalance")
    public int getAllWalletsBalance(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return walletService.getSumFromAllWallets(user.getId());
        }
        return 0;
    }
}


