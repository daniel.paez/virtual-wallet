package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.models.dtos.WalletDto;
import com.example.virtualwallet.services.contracts.CardService;
import com.example.virtualwallet.services.contracts.TransferService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import com.example.virtualwallet.services.mappers.CardMapper;
import com.example.virtualwallet.services.mappers.TransferMapper;
import com.example.virtualwallet.services.mappers.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;

@Controller
@RequestMapping("/users/{userId}/cards-and-wallets")
public class CardAndWalletMvcController {
    private final UserService userService;
    private final CardService cardService;

    private final WalletService walletService;
    private final TransferService transferService;
    private final TransferMapper transferMapper;

    private final CardMapper cardMapper;

    private final WalletMapper walletMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardAndWalletMvcController(UserService userService,
                                      CardService cardService,
                                      WalletService walletService, TransferService transferService,
                                      TransferMapper transferMapper,
                                      CardMapper cardMapper, WalletMapper walletMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.cardService = cardService;
        this.walletService = walletService;
        this.transferService = transferService;
        this.transferMapper = transferMapper;
        this.cardMapper = cardMapper;
        this.walletMapper = walletMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllUserCardsAndWallets(@PathVariable int userId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("cards", cardService.getAllUserCards(user, userId));
        model.addAttribute("wallets",walletService.getAllUserWallets(user,userId));
        model.addAttribute("newCard", new CardDto());
        model.addAttribute("newWallet", new WalletDto());
        return "profile-cards-and-wallets";
    }


    @PostMapping("/new-card")
    public String createCard(@Valid @ModelAttribute("newCard") CardDto cardDto,
                             @PathVariable int userId, BindingResult bindingResult, Model model, HttpSession session) {


        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "profile-cards-and-wallets";
        }

        try {
            Card card = cardMapper.fromDto(cardDto, user);
            cardService.create(userId,card,user);

            return "redirect:/users/{userId}/cards-and-wallets";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/new-wallet")
    public String createWallet(@Valid @ModelAttribute("newWallet") WalletDto walletDto,
                             @PathVariable int userId, BindingResult bindingResult, Model model, HttpSession session) {


        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "profile-cards-and-wallets";
        }

        try {
            Wallet wallet = walletMapper.fromDto(walletDto,user);
            walletService.create(userId,wallet,user);


            return "redirect:/users/{userId}/cards-and-wallets";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/cards/{cardId}/delete")
    public String deleteCard(@PathVariable int cardId, Model model, HttpSession session, @PathVariable int userId) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            cardService.delete(userId, cardId, user);

            return "redirect:/users/{userId}/cards-and-wallets";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/wallets/{walletId}/delete")
    public String deleteWallet(@PathVariable int walletId, Model model, HttpSession session, @PathVariable int userId) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            walletService.delete(userId,walletId,user);

            return "redirect:/users/{userId}/cards-and-wallets";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }



    @ModelAttribute("allBalance")
    public int getAllWalletsBalance(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return walletService.getSumFromAllWallets(user.getId());
        }
        return 0;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }

    @ModelAttribute("currentUserId")
    public int getCurrentUserId(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session).getId();
        }
        return 0;
    }

}

