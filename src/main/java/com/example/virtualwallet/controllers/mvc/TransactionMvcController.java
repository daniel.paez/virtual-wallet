package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.TransactionCategory;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.FilterDto;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.services.contracts.TransactionCategoryService;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.mappers.FilterMapper;
import com.example.virtualwallet.services.mappers.TransactionMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/transactions")
public class TransactionMvcController {


    private final TransactionService transactionService;

    private final TransactionCategoryService transactionCategoryService;

    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper authenticationHelper;

    private final FilterMapper filterMapper;


    public TransactionMvcController(TransactionService transactionService, TransactionCategoryService transactionCategoryService, TransactionMapper transactionMapper, AuthenticationHelper authenticationHelper, FilterMapper filterMapper) {
        this.transactionService = transactionService;
        this.transactionCategoryService = transactionCategoryService;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
        this.filterMapper = filterMapper;
    }

    @ModelAttribute("categories")
    public List<TransactionCategory> populateCategories() {
        return transactionCategoryService.getAll();
    }

    @GetMapping
    public String getAllTransactions(@ModelAttribute("filterOptions") FilterDto filterDto, Model model,
                                     HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {

            FilterOptions filterOptions = filterMapper.fromDto(filterDto);

            List<Transaction> transactions = transactionService.filterUserTransactions(filterOptions,user,user.getId());

            model.addAttribute("filterOptions",filterDto);
            model.addAttribute("transactions", transactions);

            return "transactions";
        }catch (UnauthorizedOperationException e){
            return "access-denied";
        }

    }

    @GetMapping("/new")
    public String showNewTransactionPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("transaction", new TransactionDto());
        return "transaction-new";
    }

    @PostMapping("/new")
    public String createNewTransaction(@Valid @ModelAttribute("transaction") TransactionDto transactionDto,
                                       BindingResult errors,
                                       Model model,
                                       HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "transaction-new";
        }


        Transaction transaction = transactionMapper.fromDto(transactionDto, user);
        transactionService.create(user.getId(), transaction.getSenderWallet().getId(),
                transaction, user);
        return "transaction-success";

    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public int getCurrentUserId(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getId();
        }
        return 0;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }
}
