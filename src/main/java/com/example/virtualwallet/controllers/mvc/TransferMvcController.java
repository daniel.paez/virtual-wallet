package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.NotEnoughBalanceException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransferDto;
import com.example.virtualwallet.services.contracts.TransferService;
import com.example.virtualwallet.services.mappers.TransferMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/transfers")
public class TransferMvcController {

    private final TransferService transferService;
    private final AuthenticationHelper authenticationHelper;

    private final TransferMapper transferMapper;

    public TransferMvcController(TransferService transferService, AuthenticationHelper authenticationHelper, TransferMapper transferMapper) {
        this.transferService = transferService;
        this.authenticationHelper = authenticationHelper;
        this.transferMapper = transferMapper;
    }

    @GetMapping("/new")
    public String showNewTransferPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("transfer", new TransferDto());
        return "transfer-new";
    }

    @PostMapping("/new")
    public String createNewTransfer(@Valid @ModelAttribute("transfer") TransferDto transferDto,
                                       BindingResult errors,
                                    HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "transfer-new";
        }


        try {
            Transfer transfer = transferMapper.fromDto(transferDto,user);
            transferService.create(transfer,user, user.getId());
            return "transfer-success";
        }catch (NotEnoughBalanceException e){
            return "transfer-fail";
        }catch (EntityNotFoundException e){
            return "not-found";
        }




    }

    @GetMapping
    public String getAllTransfers(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("transfers", transferService.getAllUserTransfers(user, user.getId()));
            return "transfers";
        }catch (UnauthorizedOperationException e){
            return "access-denied";
        }


    }
    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public int getCurrentUserId(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getId();
        }
        return 0;
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            return authenticationHelper.tryGetUser(session);
        }
        return null;
    }
}
