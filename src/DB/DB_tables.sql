create table transaction_categories
(
    category_id   int auto_increment
        primary key,
    category_name varchar(30) not null
);

create table user_roles
(
    user_role_id int auto_increment
        primary key,
    role_type    varchar(20) not null
);

create table users
(
    user_id           int auto_increment
        primary key,
    isBlocked         tinyint default 0 not null,
    isActive          tinyint default 1 not null,
    user_role_id      int               not null,
    username          varchar(20)       not null,
    password          varchar(300)      not null,
    profile_photo_url varchar(200)      not null,
    email             varchar(40)       not null,
    registration_date datetime          not null,
    phone_number      char(10)          not null,
    first_name        varchar(30)       not null,
    last_name         varchar(30)       not null,
    enabled           tinyint           not null,
    verification_code varchar(64)       null,
    constraint users_email_uindex
        unique (email),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_user_roles_user_role_id_fk
        foreign key (user_role_id) references user_roles (user_role_id)
);

create table cards
(
    card_id          int auto_increment
        primary key,
    card_number      char(16)    not null,
    check_number     char(3)     not null,
    owner_id         int         not null,
    card_type        varchar(20) not null,
    expire_date      date        not null,
    card_holder_name varchar(40) not null,
    constraint cards_users_user_id_fk
        foreign key (owner_id) references users (user_id)
);

create table verification_statuses
(
    verification_status_id int auto_increment
        primary key,
    verification_status    varchar(20) not null
);

create table verification_tokens
(
    id      int auto_increment
        primary key,
    token   varchar(500) not null,
    user_id int          not null,
    date    date         not null,
    constraint verification_tokens_user_id_uindex
        unique (user_id),
    constraint verification_tokens_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table verifications
(
    verification_id        int auto_increment
        primary key,
    verification_status_id int          not null,
    admin_id               int          not null,
    id_card_photo          varchar(300) null,
    selfie                 varchar(300) null,
    user_id                int          null,
    verification_date      datetime     null
);

create table wallets
(
    wallet_id      int auto_increment
        primary key,
    wallet_name    varchar(35) not null,
    wallet_balance int         not null,
    user_id        int         not null,
    isDefault      tinyint     not null
);

create table transactions
(
    transaction_id      int auto_increment
        primary key,
    transaction_amount  int               not null,
    transaction_date    datetime          not null,
    category_id         int               not null,
    sender_wallet_id    int               not null,
    recipient_wallet_id int               not null,
    isLarge             tinyint default 0 null,
    isVerified          tinyint default 1 null,
    constraint transactions_transaction_categories_category_id_fk
        foreign key (category_id) references transaction_categories (category_id),
    constraint transactions_wallets_wallet_id_fk
        foreign key (sender_wallet_id) references wallets (wallet_id),
    constraint transactions_wallets_wallet_id_fk_2
        foreign key (recipient_wallet_id) references wallets (wallet_id)
);

create table transfers
(
    transfer_id     int auto_increment
        primary key,
    transfer_amount int      not null,
    wallet_id       int      not null,
    card_id         int      not null,
    transfer_date   datetime not null,
    constraint transfers_cards_card_id_fk
        foreign key (card_id) references cards (card_id),
    constraint transfers_wallets_wallet_id_fk
        foreign key (wallet_id) references wallets (wallet_id)
);
