package com.example.virtualwalletdummyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtualWalletDummyApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtualWalletDummyApiApplication.class, args);
    }

}
