package com.example.virtualwalletdummyapi.services;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class DummyService {
    public boolean isSuccessful () {
        Random rnd = new Random();
        return rnd.nextBoolean();
    }
}
