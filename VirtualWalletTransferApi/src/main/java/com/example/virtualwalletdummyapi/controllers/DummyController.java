package com.example.virtualwalletdummyapi.controllers;

import com.example.virtualwalletdummyapi.services.DummyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/withdraw")
public class DummyController {

    private final DummyService dummyService;

    public DummyController(DummyService dummyService) {
        this.dummyService = dummyService;
    }


    @GetMapping
    public HttpStatus isSuccessful() {
        boolean isAccepted = dummyService.isSuccessful();

        if (isAccepted) {
            return HttpStatus.OK;
        }
        return HttpStatus.BAD_REQUEST;
    }
}
